import java.sql.ResultSetMetaData;

public class parseResultSet {
	
	public int parseToTable(java.sql.ResultSet rs, java.io.PrintWriter out) // parst sql resultSets in html tabelle
			    throws Exception {
			 int rowCount = 0;

			 out.println("<table class=\"table table-striped\">");
			 ResultSetMetaData rsmd = rs.getMetaData();
			 int columnCount = rsmd.getColumnCount();
			 // table header
			 out.println("<TR>");
			 for (int i = 0; i < columnCount; i++) {
			   out.println("<TH>" + rsmd.getColumnLabel(i + 1) + "</TH>");
			   }
			 out.println("</TR>");
			 // the data
			 while (rs.next()) {
			  rowCount++;
			  out.println("<TR>");
			  for (int i = 0; i < columnCount; i++) {
			    out.println("<TD>" + rs.getString(i + 1) + "</TD>");
			    }
			  out.println("</TR>");
			  }
			 out.println("</TABLE>");
			 return rowCount;
			}
	 
	 public int parseToLinktable(java.sql.ResultSet rs, java.io.PrintWriter out) // parst sql resultSets in html tabelle
			    throws Exception {
			 int rowCount = 0;

			 out.println("<form action=\"tabelle\" method=\"get\"><div class=\"btn-group-vertical\">");
			 ResultSetMetaData rsmd = rs.getMetaData();
			 int columnCount = rsmd.getColumnCount();
			 // table header
			 for (int i = 0; i < columnCount; i++) {
			   out.println("<h2>" + rsmd.getColumnLabel(i + 1) + "</h2>");
			   }
			 while (rs.next()) {
			  rowCount++;
			  for (int i = 0; i < columnCount; i++) {
				  String value = rs.getString(i + 1);
			    out.println("<button type=\"submit\" name=\"anfrage\" value=\"" + value + "\"class=\"btn btn-default\">" + value + "</button>");
			    }
			  }
			 out.println("</div></form>");
			 return rowCount;
			}
}
