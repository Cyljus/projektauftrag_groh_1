import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class Tabelle
 */
@WebServlet("/tabelle")
public class Tabelle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Tabelle() {
        super();
        }
    

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
    	PrintWriter out = response.getWriter();
    	
    	out.print(
    			"<!doctype html>"
    			+"<html lang=\"en\">"
    			+"<head>"
    			+"<title>Tabelle</title>"
    			+"<meta charset=\"utf-8\">"
    			+"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    			+"<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">"
    			+"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>"
    			+"<script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>"
    			+"</head>"
    			+"<body>"
    			+"<div class=\"row\">"
    			+"<div class=\"col-sm-4\">"
    			+"<form action=\"tabelle\" method=\"get\">"
    			+"<button type=\"submit\" name=\"anfrage\" value=\"menu\" class=\"btn btn-primary\">tablellenauswahl</button>"
    			+"</form>"
    			+ "</div>"
    			+"<div class=\"col-sm-4\">"
    			);
    	
    	String anfrage = request.getParameter("anfrage");
    	parseResultSet parser = new parseResultSet();
    	String querry = "SHOW TABLES";
    		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection( "jdbc:mysql://localhost/itknow", "root", "" );
			Statement myStmt = con.createStatement();
			
			if(anfrage == null || anfrage.equals("menu")){
				ResultSet myRs = myStmt.executeQuery(querry);
	    		parser.parseToLinktable(myRs, out);
	    		myRs.close();
				
			} else {
	    		querry = "SELECT * FROM " + anfrage;
	    		ResultSet myRs = myStmt.executeQuery(querry);
	    		parser.parseToTable(myRs, out);
	    		myRs.close();
	    	}
			out.print(
					"</div>"
					+"<div class=\"col-sm-4\"></div>"
					+"</div>"
					+"</div>"
					+"</div>"
					+"</html>"
					);
			myStmt.close();
    		}
			catch (Exception exc){
				exc.printStackTrace();
			}
		
	}

}
